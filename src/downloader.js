import logging from './logging.js';
import avr109 from './avr109.js';
import binary from './binary.js';
import intelhex from './intelhex.js';

var hexToBin = binary.hexToBin;
var binToHex = binary.binToHex;
var log = logging.log;
var kDebugError = logging.kDebugError;
var kDebugNormal = logging.kDebugNormal;
var kDebugFine = logging.kDebugFine;
var kDebugVeryFine = logging.kDebugVeryFine;

var IntelHEX = intelhex.IntelHEX;

function Downloader() {}

Downloader.prototype.downloadSketch = function(deviceName, boardObj, dest) {
  var downloader = this;
  // var boardObj = avr109.NewAvr109Board({pageSize: 128, flashSize: 32768});
  if (!boardObj.status.ok()) {
    log(kDebugError, "Couldn't create AVR109 Board: " + boardObj.status.toString());
    return;
  }

  var board = boardObj.board;
  var length = board.flashSize_;
  board.connect(deviceName, function(status) {
    if (status.ok()) {
      log(kDebugNormal, "Reading configuration...");
      board.readFlash(0, length, function(status) {
        //log(kDebugNormal, "AVR programming status: " + status.toString());
        if (board.flash_.length == length) {
          log(kDebugNormal, "Done!");
        }
        if (status.ok()) {
          HEXfile = new IntelHEX(board.flash_, 255, 0, true);
          HEXfile.createRecords();
          Array.prototype.push.apply(dest, HEXfile.records);
        }
      });
    } else {
      log(kDebugNormal, "Mod connection error: " + status.toString());
      return -1;
    }
  });
}

export var Downloader;
