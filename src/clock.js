var RealClock = function() { };

RealClock.prototype.nowMillis = function() {
  return new Date().getTime();
}

export var RealClock = RealClock;
