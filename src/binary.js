function binToHex(bin) {
  var bufferView = new Uint8Array(bin);
  var hexes = [];
  for (var i = 0; i < bufferView.length; ++i) {
    hexes.push(bufferView[i]);
  }
  return hexes;
}

function binToHexString(bin) {
  var bufferView = new Uint8Array(bin);
  var hexes = [];
  var s = '';
  var hexEncodeArray = [
          '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
        ];
  for (var i = 0; i < bufferView.length; ++i) {
    var code = bufferView[i];
    s += hexEncodeArray[code >>> 4];
    s += hexEncodeArray[code & 0x0F];
    hexes.push(s);
    s = '';
  }
  return hexes;

}

function hexToBin(hex) {
  var buffer = new ArrayBuffer(hex.length);
  var bufferView = new Uint8Array(buffer);
  for (var i = 0; i < hex.length; i++) {
    bufferView[i] = hex[i];
  }

  return buffer;
}

function hexRep(intArray) {
  var buf = "[";
  var sep = "";
  for (var i = 0; i < intArray.length; ++i) {
    var h = intArray[i].toString(16);
    if (h.length == 1) { h = "0" + h; }
    buf += (sep + "0x" + h);
    sep = ",";
  }
  buf += "]";
  return buf;
}

function storeAsTwoBytes(n) {
  var lo = (n & 0x00FF);
  var hi = (n & 0xFF00) >> 8;
  return [hi, lo];
}

export var binToHex = binToHex;
export var hexToBin = hexToBin;
export var hexRep = hexRep;
export var storeAsTwoBytes = storeAsTwoBytes;
export var binToHexString = binToHexString;
