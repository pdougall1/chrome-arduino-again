// var serialsend = require("./serialmonitor.js").doSend;
var download = require("./downloader.js");
var avr109 = require("./avr109.js");

var COMMANDS = {
    MOTOR0: "0",
    MOTOR1: "1",
    MOTOR2: "2",
    ALLMOTORSSAME: "-1",
    ALLMOTORS: "{",
    LED: "l",
    RUNPATTERN: "r",
    INCREASEPOWER: "P",
    DECREASEPOWER: "p",
    INCREASETIME: "T",
    DECREASETIME: "t",
    READINPUT: "i",
    CYCLEPATTERN: "c",
    ENDCHAR: "e", // non-int end character helps Arduino run parseInt() faster
    SEPARATOR: ","
};

var IDS = {
  MOTOR0: "motor_0",
  MOTOR1: "motor_1",
  MOTOR2: "motor_2"
};

function Mod() {
  this.init_();
}


Mod.prototype.init_ = function() {
  this.config = {
    flashSize: 28672,
    pageSize: 128,
  };
  this.board = avr109.NewAvr109Board(this.config);
  this.hex = [];
};

Mod.prototype.sendCmd = function(command,setting) {
    var cmd = command;
    if (setting) {
      cmd += COMMANDS.SEPARATOR + setting + COMMANDS.ENDCHAR;
    }
    doSend(cmd);
};

Mod.prototype.runPattern = function(patNumer) {
  doSend(COMMANDS.RUNPATTERN, patNumber < 0 ? 0 : patNumber);
};
Mod.prototype.cyclePattern = function() {
  doSend(COMMANDS.CYCLEPATTERN);
};
Mod.prototype.increasePower = function() {
  doSend(COMMANDS.INCREASEPOWER);
}
Mod.prototype.decreasePower = function() {
  doSend(COMMANDS.DECREASEPOWER);
}
Mod.prototype.increaseTime = function() {
  doSend(COMMANDS.INCREASETIME);
}
Mod.prototype.decreaseTime = function() {
  doSend(COMMANDS.DECREASETIME);
}
Mod.prototype.setLED = function(power) {
  doSend(COMMANDS.LED, power < 0 : 0 : power);
}
  //setSpeed: '',
  //setTime: '',
  //setOutput: '',
  //getInput: '',

Mod.prototype.getHex = function() {
    var downloader = new download.Downloader();
    var portMenu = document.getElementById("devices_menu");
    var selectedPort = portMenu.options[portMenu.selectedIndex].text;
    downloader.downloadSketch(selectedPort, this.board, this.hex);
};





function doSend(sendData) {
  var input = document.getElementById("todevice_data");
  var data;

  if (sendData) {
    data = sendData;
  } else {
    data = input.value;
    input.value = "";
  }

  //log(kDebugFine, "SENDING " + data + " ON CONNECTION: " + connectionId_);
  chrome.serial.send(connectionId_, stringToBinary(data), sendDone);
}

function sendDone(sendArg) {
  //log(kDebugFine, "ON SEND:" + JSON.stringify(sendArg));
  //log(kDebugFine, "SENT " + sendArg.bytesSent + " BYTES ON CONN: " + connectionId_);
}

function stringToBinary(str) {
  var buffer = new ArrayBuffer(str.length);
  var bufferView = new Uint8Array(buffer);
  for (var i = 0; i < str.length; i++) {
    bufferView[i] = str.charCodeAt(i);
  }

  return buffer;
}

// ?
// These need to be moved out.
function motorChange() {
  for (var i in IDS) {
    if (IDS[i] === this.id) {
      sendCmd(COMMANDS[i], this.value);
    }
  }
}

function patternChange() {
  sendCmd(COMMANDS.RUNPATTERN, this.value);

}

var motors = document.getElementsByClassName("mx");
for (var i = 0; i < motors.length; i++) {
  motors[i].addEventListener('change', motorChange);
}

document.getElementById("patterns_menu").addEventListener("change", patternChange);

//exports.foo = console;
exports.Mod = Mod;
